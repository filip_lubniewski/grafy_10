package graphs.utils;

import graphs.adjacency.list.Node;

import java.util.Comparator;

/**
 * Created by macbookpro on 17.05.2016.
 */
public class NodesComparator implements Comparator<Node> {
    private int[] distances;
    public NodesComparator(int[] distances) {
        this.distances = distances;
    }

    @Override
    public int compare(Node o1, Node o2) {
        return distances[o1.numberOfNode] - distances[o2.numberOfNode];
    }
}
