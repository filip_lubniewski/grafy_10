package graphs.utils;

import graphs.edge.list.Edge;

import java.util.Comparator;

/**
 * Created by macbookpro on 17.05.2016.
 */
public class EdgeComparator implements Comparator<Edge> {
    @Override
    public int compare(Edge o1, Edge o2) {
        if (o1.distance <= o2.distance) {
            return -1;
        } else {
            return 1;
        }
    }
}
