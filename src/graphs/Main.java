package graphs;

import graphs.adjacency.list.AdjacencyList;
import graphs.adjacency.matrix.AdjacencyMatrix;
import graphs.algorithms.Dijkstra;
import graphs.algorithms.Kruskal;
import graphs.edge.list.Edge;
import graphs.edge.list.EdgeList;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        AdjacencyList graphWithAdjacencyList = new AdjacencyList(5);

        graphWithAdjacencyList.add(1, 2, 10);
        graphWithAdjacencyList.add(1, 5, 30);
        graphWithAdjacencyList.add(2, 1, 15);
        graphWithAdjacencyList.add(4, 1, 20);
        graphWithAdjacencyList.displayGraph();

        EdgeList edgeList = new EdgeList();
        edgeList.add(1, 3, 3);
        edgeList.add(2, 1, 1);
        edgeList.add(2, 3, 4);
        edgeList.add(2, 4, 6);
        edgeList.add(3, 4, 5);
        edgeList.add(4, 5, 2);
        edgeList.add(5, 2, 7);
        edgeList.displayGraph();

        AdjacencyMatrix graphWithAdjacencyMatrix = new AdjacencyMatrix(5);
        graphWithAdjacencyMatrix.add(1, 2, 3);
        graphWithAdjacencyMatrix.add(1, 5, 30);
        graphWithAdjacencyMatrix.add(2, 1, 15);
        graphWithAdjacencyMatrix.add(4, 1, 20);
        graphWithAdjacencyMatrix.displayGraph();

        AdjacencyList adjacencyList = new AdjacencyList(6);
        adjacencyList.add(0, 1, 3);
        adjacencyList.add(0, 4, 3);
        adjacencyList.add(1, 2, 1);
        adjacencyList.add(2, 5, 1);
        adjacencyList.add(2, 3, 3);
        adjacencyList.add(3, 1, 3);
        adjacencyList.add(4, 5, 2);
        adjacencyList.add(5, 3, 1);
        adjacencyList.add(5, 0, 6);

        System.out.println();
        adjacencyList.displayGraph();

        Dijkstra dijkstra = new Dijkstra(adjacencyList);


        for(int i = 1; i <= 5; i++) {
            System.out.println("From 0 to " + i +  " shortest path has length " + dijkstra.searchShortestPath(0, i));
        }


        System.out.println("\nMinimum spanning tree after Kruskal algortihm: \n");
        Kruskal kruskal = new Kruskal(edgeList);
        ArrayList<Edge> resultingGraph = kruskal.getMinimumSpanningTree();
        EdgeList.displayGraph(resultingGraph);
    }
}
