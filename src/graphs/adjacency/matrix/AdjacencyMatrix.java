package graphs.adjacency.matrix;

/**
 * Created by macbookpro on 16.05.2016.
 */
public class AdjacencyMatrix {
    private int[][] graph;
    private int size;

    public AdjacencyMatrix(int size) {
        graph = new int[size + 1][size + 1];
        this.size = size;

        for (int i = 0; i <= size; i++)
            for (int j = 0; j <= size; j++) {
                graph[i][j] = -1;
            }
    }

    public void add(int startNode, int endNode, int distance) {
        graph[startNode][endNode] = distance;
    }

    public void displayGraph() {
        int spacebars = 1;
        displaySpacebars(3);

        for (int i = 1; i <= size; i++)
            System.out.print(i + "  ");

        System.out.println();

        for (int i = 1; i <= size; i++) {
            System.out.print(i);

            for (int j = 1; j <= size; j++) {
                if (graph[i][j] > -1 && graph[i][j] < 10)
                    spacebars = 2;
                else
                    spacebars = 1;

                displaySpacebars(spacebars);
                System.out.print(graph[i][j]);
            }

            System.out.println();
        }
    }

    private void displaySpacebars(int k) {
        for (int i = 0; i < k; i++) {
            System.out.print(" ");
        }
    }
}
