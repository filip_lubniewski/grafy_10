package graphs.adjacency.list;

import java.util.ArrayList;

/**
 * Created by macbookpro on 16.05.2016.
 */
public class AdjacencyList {
    public ArrayList<ArrayList<Node>> graph;
    private int quantityOfNodes;

    public AdjacencyList(int size) {
        graph = new ArrayList<>(size+1);
        quantityOfNodes = size;

        for(int i = 0; i <= size; i++) {
            graph.add(i, new ArrayList<>());
        }
    }

    public void add(int startNode, int endNode, int distance) {
        Node newNode = new Node(endNode, distance);
        graph.get(startNode).add(newNode);
    }

    public void displayGraph() {
        for(int i = 0; i < quantityOfNodes; i++) {
            System.out.print(i + ": ");

            for(int j = 0; j < graph.get(i).size(); j++) {
                Node currentNode = graph.get(i).get(j);
                System.out.print("(" + currentNode.numberOfNode + ", " + currentNode.distance + ") ");
            }

            System.out.println("\n");
        }
    }

    public int getQuantityOfNodes() {
        return quantityOfNodes;
    }
}
