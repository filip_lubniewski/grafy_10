package graphs.adjacency.list;

/**
 * Created by macbookpro on 16.05.2016.
 */
public class Node {
    public int numberOfNode;
    public int distance;

    public Node(int numberOfNode, int distance) {
        this.numberOfNode = numberOfNode;
        this.distance = distance;
    }
}
