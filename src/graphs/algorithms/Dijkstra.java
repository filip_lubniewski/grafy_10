package graphs.algorithms;

import graphs.adjacency.list.AdjacencyList;
import graphs.adjacency.list.Node;
import graphs.utils.NodesComparator;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by macbookpro on 17.05.2016.
 */
public class Dijkstra {
    private AdjacencyList list;
    private int quantityOfNodes;

    public Dijkstra(AdjacencyList graph) {
        this.list = graph;
        this.quantityOfNodes = graph.getQuantityOfNodes();
    }

    public int searchShortestPath(int source, int ending) {

        int[] distanceFromSource = new int[quantityOfNodes + 1];
        boolean[] visited = new boolean[quantityOfNodes + 1];
        boolean[] isAddedToQueue = new boolean[quantityOfNodes +1];

        for (int i = 0; i <= quantityOfNodes; i++) {
            distanceFromSource[i] = Integer.MAX_VALUE;
        }

        distanceFromSource[source] = 0;

        Comparator<Node> comparator = new NodesComparator(distanceFromSource);
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>(comparator);

        priorityQueue.add(new Node(source, 0));
        isAddedToQueue[source] = true;

        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.poll();
            isAddedToQueue[currentNode.numberOfNode] = false;

            int distance = distanceFromSource[currentNode.numberOfNode];

            if (visited[currentNode.numberOfNode]) {
                continue;
            } else {
                visited[currentNode.numberOfNode] = true;
            }

            for(int i = 0; i < list.graph.get(currentNode.numberOfNode).size(); i++) {
                Node neighbour = list.graph.get(currentNode.numberOfNode).get(i);

                if(neighbour.distance + distance < distanceFromSource[neighbour.numberOfNode]) {
                    distanceFromSource[neighbour.numberOfNode] = neighbour.distance + distance;

                    if(!isAddedToQueue[neighbour.numberOfNode])
                    {
                        priorityQueue.add(neighbour);
                        isAddedToQueue[neighbour.numberOfNode] = true;
                    }
                }
            }
        }

        return distanceFromSource[ending];

    }

}
