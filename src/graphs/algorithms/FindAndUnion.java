package graphs.algorithms;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by macbookpro on 17.05.2016.
 */
public class FindAndUnion {

    private Map<Integer, Node> map = new HashMap<>();

    public FindAndUnion() {
    }

    class Node {
        int numberOfNode;
        Node parent;
        int rank;
    }

    public void makeSet(int numberOfNode) {
        Node node = new Node();

        node.numberOfNode = numberOfNode;
        node.rank = 0;
        node.parent = node;

        map.put(numberOfNode, node);
    }

    public boolean union(int firstNode, int secondNode) {
        Node node1 = map.get(firstNode);
        Node node2 = map.get(secondNode);

        Node parent1 = findSet(node1);
        Node parent2 = findSet(node2);

        if (parent1.numberOfNode != parent2.numberOfNode) {
            if (parent1.rank >= parent2.rank) {
                parent1.rank = (parent1.rank == parent2.rank) ? parent1.rank + 1 : parent1.rank;
                parent2.parent = parent1;
            } else {
                parent1.parent = parent2;
            }

            return true;
        }

        return false;
    }

    public Node findSet(Node node) {
        Node parent = node.parent;

        if (parent == node) {
            return node;
        }

        node.parent = findSet(node.parent);
        return node.parent;
    }

    public int findSet(int numberOfNode) {
        return findSet(map.get(numberOfNode)).numberOfNode;
    }
}
