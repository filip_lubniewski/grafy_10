package graphs.algorithms;

import graphs.edge.list.Edge;
import graphs.edge.list.EdgeList;
import graphs.utils.EdgeComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 * Created by macbookpro on 17.05.2016.
 */
public class Kruskal {
    ArrayList<Edge> graph;
    Set<Integer> setOfNodes;

    public Kruskal(EdgeList edgeList) {
        graph = edgeList.list;
        setOfNodes = edgeList.setOfNodes;

    }

    public ArrayList<Edge> getMinimumSpanningTree() {

        Collections.sort(graph, new EdgeComparator());
        FindAndUnion set = new FindAndUnion();

        for (Integer node : setOfNodes) {
            set.makeSet(node);
        }

        ArrayList<Edge> resultingGraph = new ArrayList<>();

        for(Edge edge : graph) {
            int root1 = set.findSet(edge.start);
            int root2 = set.findSet(edge.end);

            if(root1 != root2) {
                resultingGraph.add(edge);
                set.union(edge.start, edge.end);
            }
        }

        return resultingGraph;
    }
}
