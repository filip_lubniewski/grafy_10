package graphs.edge.list;

/**
 * Created by macbookpro on 16.05.2016.
 */
public class Edge {
    public int start;
    public int end;
    public int distance;

    public Edge(int start, int end, int distance) {
        this.start = start;
        this.end = end;
        this.distance = distance;
    }
}
