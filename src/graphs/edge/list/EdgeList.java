package graphs.edge.list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by macbookpro on 16.05.2016.
 */
public class EdgeList {
    public ArrayList<Edge> list = new ArrayList<>();
    public Set<Integer> setOfNodes = new HashSet<>();

    public void add(int startNode, int endNode, int distance) {
        list.add(new Edge(startNode, endNode, distance));
        setOfNodes.add(startNode);
        setOfNodes.add(endNode);
    }

    public void displayGraph() {
        for (int i = 0; i < list.size(); i++) {
            Edge currentEdge = list.get(i);

            System.out.println(currentEdge.start + " --" + currentEdge.distance + "--> " + currentEdge.end + "\n");
        }
    }

    public static void displayGraph(List<Edge> list) {
        for (int i = 0; i < list.size(); i++) {
            Edge currentEdge = list.get(i);

            System.out.println(currentEdge.start + " --" + currentEdge.distance + "--> " + currentEdge.end + "\n");
        }
    }
}
